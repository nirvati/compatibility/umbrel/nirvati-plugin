# Nirvati plugin for Umbrel compatibility

This Nirvati plugin implements compatibility with Umbrel.

It is a proxy around the [sandboxed runtime](https://gitlab.com/nirvati/compat/umbrel/sandboxed-runtime),
which implements app downloads, as well as converting them to Nirvati's format to the extend that's possible.

![Nirvati Umbrel compatibility architecture](./architecture.png)