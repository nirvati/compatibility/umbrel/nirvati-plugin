use std::path::PathBuf;
use tonic::transport::Uri;

pub mod repo;
pub mod runtime;

#[derive(Clone)]
pub struct ApiServer {
    apps_path: PathBuf,
    runtime_server_endpoint: Uri,
    user_id: String,
}

impl ApiServer {
    pub fn new(apps_path: PathBuf, runtime_server_endpoint: Uri, user_id: String) -> Self {
        Self {
            apps_path,
            runtime_server_endpoint,
            user_id,
        }
    }
}
