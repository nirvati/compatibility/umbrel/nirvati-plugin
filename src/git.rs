// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::path::Path;

use anyhow::Result;
use gix::bstr::BStr;
use gix::prepare_clone;
use gix::refspec::parse::Operation;

// TODO: Reimplement this for gix
fn repo_checkout_branch(repo: &Path, branch: &str) -> Result<()> {
    let repo = git2::Repository::open(repo)?;
    let mut checkout_builder = git2::build::CheckoutBuilder::new();
    checkout_builder.force();
    repo.checkout_head(Some(&mut checkout_builder))?;
    let branch_ref = format!("refs/remotes/origin/{}", branch);
    let branch_ref = repo.find_reference(&branch_ref)?;
    let branch_commit = branch_ref.peel_to_commit()?;
    repo.checkout_tree(&branch_commit.into_object(), Some(&mut checkout_builder))?;
    repo.set_head(branch_ref.name().unwrap())?;
    Ok(())
}

pub fn clone(repo_url: gix::Url, branch: &str, target: &Path) -> Result<()> {
    tracing::debug!("Cloning {repo_url:?} into {target:?}...");
    let prepare_clone = prepare_clone(repo_url, target)?;
    let rspec = format!("refs/heads/{}", branch);
    let target_branch_refspec =
        gix::refspec::parse(BStr::new(&rspec), Operation::Fetch)?.to_owned();
    let (mut prepare_checkout, _) = prepare_clone
        .with_fetch_options(gix::remote::ref_map::Options {
            extra_refspecs: vec![target_branch_refspec],
            ..Default::default()
        })
        .fetch_then_checkout(gix::progress::Discard, &gix::interrupt::IS_INTERRUPTED)?;
    let _ =
        prepare_checkout.main_worktree(gix::progress::Discard, &gix::interrupt::IS_INTERRUPTED)?;
    repo_checkout_branch(target, branch)?;
    Ok(())
}
