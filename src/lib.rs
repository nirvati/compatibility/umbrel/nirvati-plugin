pub mod grpc {
    tonic::include_proto!("nirvati_app_plugin");
}

pub mod runtime_grpc {
    tonic::include_proto!("nirvati_umbrel_runtime");
}

pub mod types;
pub mod api;
pub(crate) mod git;
