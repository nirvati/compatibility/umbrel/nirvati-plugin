use nirvati_plugin_umbrel::api::ApiServer;
#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let server = ApiServer::new("/apps".into(), "http://localhost:50051".parse().unwrap(), "user".into());
    let addr = "[::]:50051".parse().unwrap();
    tonic::transport::Server::builder()
        .add_service(nirvati_plugin_umbrel::grpc::runtime_plugin_server::RuntimePluginServer::new(server.clone()))
        .add_service(nirvati_plugin_umbrel::grpc::source_plugin_server::SourcePluginServer::new(server))
        .serve(addr)
        .await
        .unwrap();
}
