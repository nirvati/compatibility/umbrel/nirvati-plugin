use crate::api::ApiServer;
use crate::grpc::source_plugin_server::SourcePlugin;
use crate::grpc::{
    AppUpdate, DownloadAppRequest, DownloadAppResponse, DownloadAppsRequest, DownloadAppsResponse,
    GetAllUpdatesRequest, GetAllUpdatesResponse, GetAppStoreMetadataRequest,
    GetAppStoreMetadataResponse, GetUpdateRequest, GetUpdateResponse,
};
use crate::types::UmbrelMetadata;
use anyhow::anyhow;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, HashMap};
use std::str::FromStr;
use tonic::{Request, Response, Status};

struct RepoSrc {
    repo_url: gix::url::Url,
    branch: String,
}

#[derive(Serialize, Deserialize)]
struct PluginState {
    versions: BTreeMap<String, String>,
}

impl FromStr for RepoSrc {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (repo, branch) = s.split_once('#').ok_or(anyhow!("Failed to parse repo"))?;
        let repo_url = gix::url::parse(repo.as_ref())?;
        Ok(RepoSrc {
            repo_url,
            branch: branch.to_string(),
        })
    }
}

#[tonic::async_trait]
impl SourcePlugin for ApiServer {
    async fn download_app(
        &self,
        request: Request<DownloadAppRequest>,
    ) -> Result<Response<DownloadAppResponse>, Status> {
        let request = request.into_inner();
        let tmpdir = tempfile::tempdir().unwrap();
        let repo_src = request
            .source
            .parse::<RepoSrc>()
            .map_err(|err| Status::internal(err.to_string()))?;
        crate::git::clone(repo_src.repo_url, &repo_src.branch, &tmpdir.path())
            .map_err(|err| Status::internal(err.to_string()))?;
        // Compress the <request.app_id> subdirectory into a tar.gz
        let buf_writer = Vec::new();
        let enc = flate2::write::GzEncoder::new(buf_writer, flate2::Compression::default());
        let mut tar = tar::Builder::new(enc);
        tar.append_dir_all("", tmpdir.path().join(request.app_id))
            .map_err(|err| Status::internal(err.to_string()))?;
        tar.finish()
            .map_err(|err| Status::internal(err.to_string()))?;
        let enc = tar.into_inner().unwrap();
        let buf_writer = enc
            .finish()
            .map_err(|err| Status::internal(err.to_string()))?;
        Ok(Response::new(DownloadAppResponse {
            app_directory: buf_writer,
            state: "".to_string(),
        }))
    }

    async fn download_all_apps(
        &self,
        request: Request<DownloadAppsRequest>,
    ) -> Result<Response<DownloadAppsResponse>, Status> {
        let request = request.into_inner();
        let tmpdir = tempfile::tempdir().unwrap();
        let repo_src = request
            .source
            .parse::<RepoSrc>()
            .map_err(|err| Status::internal(err.to_string()))?;
        crate::git::clone(repo_src.repo_url, &repo_src.branch, &tmpdir.path())
            .map_err(|err| Status::internal(err.to_string()))?;
        let entries = std::fs::read_dir(&tmpdir.path())
            .map_err(|err| Status::internal(err.to_string()))?
            .map(|entry| entry.unwrap())
            .collect::<Vec<_>>();
        let mut versions = BTreeMap::new();
        for entry in entries.iter() {
            if entry.file_type().unwrap().is_dir() {
                let app_id = entry.file_name().to_string_lossy().to_string();
                // Check if umbrel-metadata.yml and a docker-compose.yml exist
                let metadata_path = entry.path().join("umbrel-metadata.yml");
                let compose_path = entry.path().join("docker-compose.yml");
                if !metadata_path.exists() || !compose_path.exists() {
                    // Delete the dir
                    std::fs::remove_dir_all(entry.path())
                        .map_err(|err| Status::internal(err.to_string()))?;
                }
                // Parse the metadata file
                let metadata = std::fs::read_to_string(metadata_path)
                    .map_err(|err| Status::internal(err.to_string()))?;
                let metadata = serde_yaml::from_str::<UmbrelMetadata>(&metadata)
                    .map_err(|err| Status::internal(err.to_string()))?;
                versions.insert(app_id, metadata.version.clone());
                continue;
            }
            std::fs::remove_file(entry.path()).map_err(|err| Status::internal(err.to_string()))?;
        }
        std::fs::remove_dir_all(tmpdir.path().join(".git"))
            .map_err(|err| Status::internal(err.to_string()))?;
        let buf_writer = Vec::new();
        let enc = flate2::write::GzEncoder::new(buf_writer, flate2::Compression::default());
        let mut tar = tar::Builder::new(enc);
        tar.append_dir_all("", tmpdir.path())
            .map_err(|err| Status::internal(err.to_string()))?;
        tar.finish()
            .map_err(|err| Status::internal(err.to_string()))?;
        let enc = tar.into_inner().unwrap();
        let buf_writer = enc
            .finish()
            .map_err(|err| Status::internal(err.to_string()))?;
        let state = serde_yaml::to_string(&PluginState { versions })
            .map_err(|err| Status::internal(err.to_string()))?;
        Ok(Response::new(DownloadAppsResponse {
            apps_directory: buf_writer,
            state,
        }))
    }

    async fn get_all_updates(
        &self,
        request: Request<GetAllUpdatesRequest>,
    ) -> Result<Response<GetAllUpdatesResponse>, Status> {
        let request = request.into_inner();
        let tmpdir = tempfile::tempdir().unwrap();
        let repo_src = request
            .source
            .parse::<RepoSrc>()
            .map_err(|err| Status::internal(err.to_string()))?;
        crate::git::clone(repo_src.repo_url, &repo_src.branch, &tmpdir.path())
            .map_err(|err| Status::internal(err.to_string()))?;
        let entries = std::fs::read_dir(&tmpdir.path())
            .map_err(|err| Status::internal(err.to_string()))?
            .map(|entry| entry.unwrap())
            .collect::<Vec<_>>();
        let state = serde_yaml::from_str::<PluginState>(&request.state)
            .map_err(|err| Status::internal(err.to_string()))?;
        let mut updates = HashMap::new();
        for entry in entries.iter() {
            if entry.file_type().unwrap().is_dir() {
                let app_id = entry.file_name().to_string_lossy().to_string();
                // Check if umbrel-metadata.yml and a docker-compose.yml exist
                let metadata_path = entry.path().join("umbrel-metadata.yml");
                let compose_path = entry.path().join("docker-compose.yml");
                if !metadata_path.exists() || !compose_path.exists() {
                    continue;
                }
                // Parse the metadata file
                let metadata = std::fs::read_to_string(metadata_path)
                    .map_err(|err| Status::internal(err.to_string()))?;
                let metadata = serde_yaml::from_str::<UmbrelMetadata>(&metadata)
                    .map_err(|err| Status::internal(err.to_string()))?;
                if state.versions.get(&app_id) != Some(&metadata.version) {
                    // Add to updates
                    updates.insert(
                        app_id,
                        AppUpdate {
                            new_version: metadata.version.clone(),
                            release_notes: serde_json::to_string(&BTreeMap::from([(
                                metadata.version,
                                BTreeMap::from([(
                                    "en".to_string(),
                                    metadata.release_notes.clone(),
                                )]),
                            )]))
                            .map_err(|err| Status::internal(err.to_string()))?,
                        },
                    );
                }
            }
        }
        Ok(Response::new(GetAllUpdatesResponse { updates }))
    }

    async fn get_update(
        &self,
        request: Request<GetUpdateRequest>,
    ) -> Result<Response<GetUpdateResponse>, Status> {
        let request = request.into_inner();
        let tmpdir = tempfile::tempdir().unwrap();
        let repo_src = request
            .source
            .parse::<RepoSrc>()
            .map_err(|err| Status::internal(err.to_string()))?;
        crate::git::clone(repo_src.repo_url, &repo_src.branch, &tmpdir.path())
            .map_err(|err| Status::internal(err.to_string()))?;
        let state = serde_yaml::from_str::<PluginState>(&request.state)
            .map_err(|err| Status::internal(err.to_string()))?;
        let metadata_path = tmpdir
            .path()
            .join(&request.app_id)
            .join("umbrel-metadata.yml");
        let metadata = std::fs::read_to_string(metadata_path)
            .map_err(|err| Status::internal(err.to_string()))?;
        let metadata = serde_yaml::from_str::<UmbrelMetadata>(&metadata)
            .map_err(|err| Status::internal(err.to_string()))?;
        if state.versions.get(&request.app_id) == Some(&metadata.version) {
            return Ok(Response::new(GetUpdateResponse { update: None }));
        } else {
            Ok(Response::new(GetUpdateResponse {
                update: Some(AppUpdate {
                    new_version: metadata.version.clone(),
                    release_notes: serde_json::to_string(&BTreeMap::from([(
                        metadata.version,
                        BTreeMap::from([("en".to_string(), metadata.release_notes.clone())]),
                    )]))
                    .map_err(|err| Status::internal(err.to_string()))?,
                }),
            }))
        }
    }

    async fn get_app_store_metadata(
        &self,
        request: Request<GetAppStoreMetadataRequest>,
    ) -> Result<Response<GetAppStoreMetadataResponse>, Status> {
        let request = request.into_inner();
        let tmpdir = tempfile::tempdir().unwrap();
        let repo_src = request
            .source
            .parse::<RepoSrc>()
            .map_err(|err| Status::internal(err.to_string()))?;
        crate::git::clone(repo_src.repo_url, &repo_src.branch, &tmpdir.path())
            .map_err(|err| Status::internal(err.to_string()))?;
        let metadata_path = tmpdir.path().join("umbrel-app-store.yml");
        return if !metadata_path.exists() && !request.source.contains("getumbrel/umbrel-apps") {
            Err(Status::not_found("Metadata file not found"))
        } else if request.source.contains("getumbrel/umbrel-apps") {
            Ok(Response::new(GetAppStoreMetadataResponse {
                name: HashMap::from([("en".to_string(), "Umbrel App Store".to_string())]),
                tagline: HashMap::from([("en".to_string(), "The default app store from umbrelOS".to_string())]),
                description: HashMap::from([("en".to_string(), "This is the default app store from umbrelOS, built by Umbrel, Inc.. Please note that on Nirvati, you are running the apps in an unofficial environment. Please do not contact Umbrel, Inc. or anyone else affiliated with Umbrel for support regarding this.".to_string())]),
                icon: "https://official-app-icons.nirvati.org/umbrel-compat/icon.svg".to_string(),
                developers: HashMap::from([("Umbrel, Inc.".to_string(), "https://umbrel.com".to_string())]),
                license: "Unknown".to_string(),
            }))
        } else {
            let parsed_metadata = std::fs::read_to_string(metadata_path)
                .map_err(|err| Status::internal(err.to_string()))?;
            let parsed_metadata = serde_yaml::from_str::<serde_yaml::Mapping>(&parsed_metadata)
                .map_err(|err| Status::internal(err.to_string()))?;
            let name = parsed_metadata
                .get(&serde_yaml::Value::String("name".to_string()))
                .ok_or(Status::internal("Name not found"))?
                .as_str()
                .ok_or(Status::internal("Name not a string"))?;
            Ok(Response::new(GetAppStoreMetadataResponse {
                name: HashMap::from([("en".to_string(), name.to_string())]),
                tagline: HashMap::from([("en".to_string(), "An app store built for umbrelOS".to_string())]),
                description: HashMap::from([("en".to_string(), "This is an app store for umbrelOS. Please note that on Nirvati, you are running the apps in an unofficial environment. Please do not contact the developers of this app store, Umbrel, Inc. or anyone else affiliated with Umbrel for support regarding this.".to_string())]),
                icon: "https://official-app-icons.nirvati.org/umbrel-compat/icon.svg".to_string(),
                developers: HashMap::from([("Unknown".to_string(), "https://nirvati.org/".to_string())]),
                license: "Unknown".to_string(),
            }))
        };
    }
}
