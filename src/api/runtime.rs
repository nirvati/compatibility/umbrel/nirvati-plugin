use crate::api::ApiServer;
use crate::grpc::runtime_plugin_server::RuntimePlugin;
use crate::grpc::{
    InstallAppRequest, InstallAppResponse, IsSupportedAppRequest, IsSupportedAppResponse,
    ParseAppRequest, ParseAppResponse, PauseAppRequest, PauseAppResponse, UninstallAppRequest,
    UninstallAppResponse, UnpauseAppRequest, UnpauseAppResponse,
};
use crate::runtime_grpc::umbrel_runtime_client::UmbrelRuntimeClient;
use crate::runtime_grpc::LoadAppComposeRequest;
use crate::types::UmbrelMetadata;
use compose_spec::service::ports::Protocol;
use compose_spec::ShortOrLong;
use flate2::read::GzDecoder;
use k8s_openapi::api::core::v1::{Service, ServicePort, ServiceSpec};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use nirvati_apps_core::internal::{Ingress, InternalAppRepresentation};
use nirvati_apps_core::metadata::{AppType, OutputMetadata, Runtime};
use semver::Version;
use slugify::slugify;
use std::collections::BTreeMap;
use std::io::{Cursor, Read};
use std::net::IpAddr;
use std::str::FromStr;
use tonic::{Request, Response, Status};

fn get_category_human_name(category: &str) -> &str {
    match category {
        "files" => "Files & Productivity",
        "bitcoin" => "Bitcoin",
        "finance" => "Finance",
        "media" => "Media",
        "networking" => "Networking",
        "social" => "Social",
        "automation" => "Home & Automation",
        "ai" => "AI",
        category => category,
    }
}
#[tonic::async_trait]
impl RuntimePlugin for ApiServer {
    async fn is_supported_app(
        &self,
        request: Request<IsSupportedAppRequest>,
    ) -> Result<Response<IsSupportedAppResponse>, Status> {
        let request = request.into_inner();
        let parser = GzDecoder::new(Cursor::new(request.app_directory));
        let mut archive = tar::Archive::new(parser);
        let entries = archive
            .entries()
            .or_else(|_| Err(Status::internal("Failed to read archive")))?
            .map(|entry| entry.or_else(|_| Err(Status::internal("Failed to read entry"))))
            .collect::<Result<Vec<_>, _>>()?;
        let mut has_metadata = false;
        let mut is_supported = false;
        'outer: for entry in entries {
            let filename = entry
                .path()
                .or_else(|_| Err(Status::internal("Failed to get entry path")))?;
            let filename = filename
                .to_str()
                .ok_or_else(|| Status::internal("Failed to convert path to string"))?;
            if filename == "umbrel-metadata.yml" {
                has_metadata = true;
                break;
            }
            if filename == "docker-compose.yml" {
                let compose = entry
                    .bytes()
                    .collect::<Result<Vec<_>, _>>()
                    .or_else(|_| Err(Status::internal("Failed to read compose file")))?;
                let compose = String::from_utf8(compose)
                    .or_else(|_| Err(Status::internal("Failed to convert compose to string")))?;
                let parsed_compose = serde_yaml::from_str::<serde_yaml::Value>(&compose)
                    .or_else(|_| Err(Status::internal("Failed to parse compose file")))?;
                if let serde_yaml::Value::Mapping(compose) = parsed_compose {
                    if let Some(serde_yaml::Value::Mapping(services)) = compose.get("services") {
                        for (_, service) in services {
                            if let serde_yaml::Value::Mapping(service) = service {
                                if service.get("network_mode").is_some() {
                                    is_supported = false;
                                    break 'outer;
                                } else if service.get("privileged").is_some() {
                                    is_supported = false;
                                    break 'outer;
                                } else if service.get("devices").is_some() {
                                    is_supported = false;
                                    break 'outer;
                                }
                            }
                        }
                    }
                }
                is_supported = true;
            }
            if is_supported && has_metadata {
                break;
            }
        }

        Ok(Response::new(IsSupportedAppResponse {
            supported: is_supported && has_metadata,
        }))
    }

    async fn parse_app(
        &self,
        request: Request<ParseAppRequest>,
    ) -> Result<Response<ParseAppResponse>, Status> {
        let request = request.into_inner();
        let parser = GzDecoder::new(Cursor::new(request.app_directory));
        let mut archive = tar::Archive::new(parser);
        // Extract archive to self.umbrel_apps_path
        archive
            .unpack(&self.apps_path.join(&request.app_id))
            .or_else(|_| Err(Status::internal("Failed to unpack archive")))?;
        let metadata = std::fs::read_to_string(
            self.apps_path
                .join(&request.app_id)
                .join("umbrel-metadata.yml"),
        )
        .or_else(|_| Err(Status::internal("Failed to read metadata file")))?;
        let metadata = serde_yaml::from_str::<UmbrelMetadata>(&metadata)
            .or_else(|_| Err(Status::internal("Failed to parse metadata file")))?;

        let mut runtime_client = UmbrelRuntimeClient::connect(self.runtime_server_endpoint.clone())
            .await
            .map_err(|_| Status::internal("Failed to connect to runtime server"))?;
        let compose = runtime_client
            .load_app_compose(Request::new(LoadAppComposeRequest {
                app_id: request.app_id.clone(),
            }))
            .await?
            .into_inner()
            .compose;

        let parsed_compose = serde_yaml::from_str::<compose_spec::Compose>(&compose)
            .map_err(|_| Status::internal("Failed to parse docker-compose.yml"))?;

        let version = Version::parse(&metadata.version).unwrap_or_else(|_| {
            Version::parse(&format!("v0.1.0+{}", slugify!(&metadata.version))).unwrap()
        });
        let output_metadata = OutputMetadata {
            id: request.app_id.clone(),
            name: metadata.name,
            version: version.clone(),
            display_version: metadata.version,
            category: nirvati::utils::MultiLanguageItem(BTreeMap::from([(
                "en".to_string(),
                get_category_human_name(&metadata.category).to_string(),
            )])),
            tagline: nirvati::utils::MultiLanguageItem(BTreeMap::from([(
                "en".to_string(),
                metadata.tagline,
            )])),
            developers: Default::default(),
            description: nirvati::utils::MultiLanguageItem(BTreeMap::from([(
                "en".to_string(),
                metadata.description,
            )])),
            dependencies: vec![],
            has_permissions: vec![],
            exposes_permissions: vec![],
            repos: BTreeMap::from([("Source code".to_string(), metadata.repo)]),
            support: metadata.support,
            gallery: metadata
                .gallery
                .into_iter()
                .map(|s| {
                    if s.starts_with("http") {
                        s
                    } else {
                        format!(
                            "https://getumbrel.github.io/umbrel-apps-gallery/{}/{}",
                            request.app_id, s
                        )
                    }
                })
                .collect(),
            icon: Some(metadata.icon.unwrap_or(format!(
                "https://getumbrel.github.io/umbrel-apps-gallery/{}/icon.svg",
                request.app_id,
            ))),
            path: metadata.path,
            default_username: metadata.default_username,
            default_password: metadata.default_password,
            implements: None,
            release_notes: if let Some(release_notes) = metadata.release_notes {
                BTreeMap::from([(
                    version.to_string(),
                    BTreeMap::from([("en".to_string(), release_notes)]),
                )])
            } else {
                BTreeMap::new()
            },
            license: "Unknown".to_string(),
            settings: Default::default(),
            single_https_domain: false,
            allow_domain_change: false,
            volumes: Default::default(),
            ports: vec![],
            exported_data: None,
            runtime: Runtime::Plugin("umbrel-compat".to_string()),
            r#type: AppType::App,
        };
        let mut err = Ok(());
        let svc_ports = parsed_compose
            .services
            .iter()
            .flat_map(|(svc_id, svc)| {
                svc.ports
                    .iter()
                    .filter_map(|port| match port {
                        ShortOrLong::Short(s) => {
                            if s.host_ip.is_some_and(|host_ip| {
                                host_ip != IpAddr::from_str("0.0.0.0").unwrap()
                                    || host_ip != IpAddr::from_str("[::]").unwrap()
                            }) {
                                return None;
                            };
                            if let Some(ref proto) = s.protocol {
                                if let Protocol::Other(ref other) = proto {
                                    err = Err(Status::internal(format!("Unsupported protocol: {}", other)));
                                    return None;
                                }
                            };
                            let host_range = s.ranges.host()?;
                            let container_range = s.ranges.container();
                            if host_range.size() > 0 && host_range.size() != container_range.size() {
                                err = Err(Status::internal(format!("Host port range size does not match container port range size for container {}", svc_id)));
                            }
                            Some(host_range.into_iter().zip(container_range.into_iter()).map(|(host_port, container_port)|
                            ServicePort {
                                app_protocol: None,
                                name: None,
                                node_port: Some(host_port as i32),
                                port: container_port as i32,
                                protocol: None,
                                target_port: None,
                            }).collect::<Vec<ServicePort>>()
                            )
                        }
                        ShortOrLong::Long(l) => {
                            if l.host_ip.is_some_and(|host_ip| {
                                host_ip != IpAddr::from_str("0.0.0.0").unwrap()
                                    || host_ip != IpAddr::from_str("[::]").unwrap()
                            }) {
                                return None;
                            };
                            if let Some(ref proto) = l.protocol {
                                if let Protocol::Other(ref other) = proto {
                                    err = Err(Status::internal(format!("Unsupported protocol: {}", other)));
                                    return None;
                                }
                            };

                            let host_range = l.published?;
                            if host_range.size() != 1 {
                                err = Err(Status::internal(format!("Host port range size does not match container port range size for container {}", svc_id)));
                            }
                            Some(vec![ServicePort {
                                    app_protocol: l.app_protocol.clone(),
                                    name: l.name.clone(),
                                    node_port: Some(host_range.start() as i32),
                                    port: l.target as i32,
                                    protocol: l.protocol.as_ref().map(|proto| match proto {
                                        Protocol::Tcp => "TCP".to_string(),
                                        Protocol::Udp => "UDP".to_string(),
                                        Protocol::Other(_) => unreachable!(),
                                    }),
                                    target_port: None,
                                }])
                        }
                    })
                    .flatten()
                    .collect::<Vec<ServicePort>>()
            })
            .collect::<Vec<ServicePort>>();
        let service = Service {
            metadata: ObjectMeta {
                name: Some(format!("app-{}", request.app_id)),
                namespace: Some(format!("{}-umbrel-compat", self.user_id)),
                ..Default::default()
            },
            spec: Some(ServiceSpec {
                type_: Some("LoadBalancer".to_string()),
                ports: Some(svc_ports),
                ..Default::default()
            }),
            status: None,
        };
        let ingress_svc = Service {
            metadata: ObjectMeta {
                name: Some(format!("app-ingress-{}", request.app_id)),
                namespace: Some(format!("{}-umbrel-compat", self.user_id)),
                ..Default::default()
            },
            spec: Some(ServiceSpec {
                type_: Some("ClusterIP".to_string()),
                ports: Some(vec![ServicePort {
                    app_protocol: None,
                    name: None,
                    node_port: None,
                    port: metadata.port as i32,
                    protocol: None,
                    target_port: None,
                }]),
                ..Default::default()
            }),
            status: None,
        };
        let ingress_svc = serde_yaml::to_value(&ingress_svc).unwrap();
        let svc = serde_yaml::to_value(&service).unwrap();
        let app = InternalAppRepresentation {
            metadata: output_metadata,
            containers: Default::default(),
            services: vec![],
            ingress: vec![Ingress {
                target_service: format!("app-ingress-{}", request.app_id),
                target_app: Some("umbrel-compat".to_string()),
                path_prefix: None,
                target_port: metadata.port,
                enable_compression: true,
                strip_prefix: false,
            }],
            plugins: vec![],
            secrets: vec![],
            custom_resources: vec![],
            other: vec![svc, ingress_svc],
        };
        Ok(Response::new(ParseAppResponse {
            parsed_app: serde_yaml::to_string(&app).unwrap(),
        }))
    }

    async fn install_app(
        &self,
        request: Request<InstallAppRequest>,
    ) -> Result<Response<InstallAppResponse>, Status> {
        let request = request.into_inner();
        let mut runtime_client = UmbrelRuntimeClient::connect(self.runtime_server_endpoint.clone())
            .await
            .map_err(|_| Status::internal("Failed to connect to runtime server"))?;
        let parser = GzDecoder::new(Cursor::new(request.app_directory));
        let mut archive = tar::Archive::new(parser);
        // Extract archive to self.umbrel_apps_path
        archive
            .unpack(&self.apps_path.join(&request.app_id))
            .or_else(|_| Err(Status::internal("Failed to unpack archive")))?;
        runtime_client
            .install_app(crate::runtime_grpc::InstallAppRequest {
                app_id: request.app_id,
            })
            .await
            .map_err(|_| Status::internal("Failed to install app"))?;
        Ok(Response::new(InstallAppResponse {}))
    }

    async fn uninstall_app(
        &self,
        request: Request<UninstallAppRequest>,
    ) -> Result<Response<UninstallAppResponse>, Status> {
        let request = request.into_inner();
        let mut runtime_client = UmbrelRuntimeClient::connect(self.runtime_server_endpoint.clone())
            .await
            .map_err(|_| Status::internal("Failed to connect to runtime server"))?;
        runtime_client
            .uninstall_app(crate::runtime_grpc::UninstallAppRequest {
                app_id: request.app_id,
            })
            .await?;
        Ok(Response::new(UninstallAppResponse {}))
    }

    async fn pause_app(
        &self,
        request: Request<PauseAppRequest>,
    ) -> Result<Response<PauseAppResponse>, Status> {
        let request = request.into_inner();
        let mut runtime_client = UmbrelRuntimeClient::connect(self.runtime_server_endpoint.clone())
            .await
            .map_err(|_| Status::internal("Failed to connect to runtime server"))?;
        runtime_client
            .stop_app(crate::runtime_grpc::StopAppRequest {
                app_id: request.app_id,
                timeout_msec: 30_000,
            })
            .await?;
        Ok(Response::new(PauseAppResponse {}))
    }

    async fn unpause_app(
        &self,
        request: Request<UnpauseAppRequest>,
    ) -> Result<Response<UnpauseAppResponse>, Status> {
        let request = request.into_inner();
        let mut runtime_client = UmbrelRuntimeClient::connect(self.runtime_server_endpoint.clone())
            .await
            .map_err(|_| Status::internal("Failed to connect to runtime server"))?;
        runtime_client
            .start_app(crate::runtime_grpc::StartAppRequest {
                app_id: request.app_id,
            })
            .await?;
        Ok(Response::new(UnpauseAppResponse {}))
    }
}
