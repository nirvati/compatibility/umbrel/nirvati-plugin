use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct UmbrelMetadata {
    #[serde(rename = "manifestVersion")]
    pub manifest_version: f32,
    pub id: String,
    pub category: String,
    pub name: String,
    pub version: String,
    pub tagline: String,
    pub description: String,
    pub developer: String,
    pub website: String,
    pub dependencies: Vec<String>,
    pub repo: String,
    pub support: String,
    pub port: u16,
    pub gallery: Vec<String>,
    pub path: Option<String>,
    #[serde(rename = "defaultUsername")]
    pub default_username: Option<String>,
    #[serde(rename = "defaultPassword")]
    pub default_password: Option<String>,
    #[serde(rename = "releaseNotes")]
    pub release_notes: Option<String>,
    pub widgets: Vec<serde_yaml::Value>,
    pub submitter: Option<String>,
    pub submission: Option<String>,
    pub icon: Option<String>,
    #[serde(rename = "deterministicPassword")]
    pub deterministic_password: Option<bool>,
}
