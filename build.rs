// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub fn main() {
    tonic_build::configure()
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile(
            &[
                "protos/plugin.proto",
                "protos/runtime.proto",
            ],
            &["protos"],
        )
        .unwrap();
}
